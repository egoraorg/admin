import React, { useState } from "react";
import { render } from "react-dom";
import { ApolloClient, InMemoryCache, ApolloProvider, } from '@apollo/client';
import Header from './components/Header';
import Login from './components/Login';
import UserInfo from './components/UserInfo';
import Users from './components/Users';
import SetUserAdmin from './components/SetUserAdmin';
import SetUserPowerLevel from './components/SetUserPowerLevel';
import DeactivateUser from './components/DeactivateUser';
import DeleteRoom from './components/DeleteRoom';
import DeleteCommunity from './components/DeleteCommunity';
import './App.css';

const client = new ApolloClient({
  uri: 'http://localhost:4000',
  cache: new InMemoryCache()
});

function App() {
  const [user, setUser] = useState(null);

  function onLogin(login) {
    setUser({name: login.user_id, accessToken: login.access_token});
  }

  return (
    <ApolloProvider client={client}>
      <div>
        <Header user={user} />
        <Login onLogin={onLogin} />
        <Users />
        <UserInfo />
        <SetUserAdmin />
        <SetUserPowerLevel />
        <DeactivateUser />
        <DeleteRoom />
        <DeleteCommunity />
      </div>
    </ApolloProvider>
  );
}

render(<App />, document.getElementById("root"));
