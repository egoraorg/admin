import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';

const initialState = {};
const middleware = [];
const store = createStore(reducers, initialState, applyMiddleware(...middleware));

export default store;
