import React, { useState } from 'react';
import { gql, ApolloClient, useApolloClient, useMutation } from "@apollo/client";

const DELETE_ROOM = gql`
  mutation DeleteRoom($roomId: String) {
    deleteRoom(roomId: $roomId) {
      kicked_users
      failed_to_kick_users
    }
  }
`;

export default function DeleteRoom({  }) {
  const [show, setShow] = useState(false);
  const [roomId, setRoomId] = useState(null);
  const [kickedUsers, setKickedUsers] = useState([]);
  const [failedToKickUsers, setFailedToKickUsers] = useState([]);
  const [deletedRoom, setDeletedRoom] = useState(null);
  const client = useApolloClient();
  
  const [deleteRoom, { loading, error }] = useMutation(DELETE_ROOM, {
    onCompleted({ deleteRoom }) {
      setDeletedRoom(roomId);
      setKickedUsers(deleteRoom.kicked_users);
      setFailedToKickUsers(deleteRoom.failed_to_kick_users);
    },
    onError(err) {
      setDeletedRoom(err.toString());
    },
  });

  return (
    <div> 
      <span>Show Delete Room</span>
      <input type="checkbox" name="show" value={show} onChange={(e) => { setShow(e.target.value); }} placeholder="show"/>
      {show && <div> 
        <input type="text" name="roomId" onChange={(e) => { setRoomId(e.target.value); }} placeholder="roomId"/>
        <button onClick={() => deleteRoom({ variables: { roomId } })}>Delete</button>
        {deletedRoom != null && <div> 
          <span>Deleted Room: {deletedRoom} </span>
          <span>Kicked Users: {kickedUsers} </span>
          <span>Failed to kick Users: {failedToKickUsers} </span>
        </div>}
      </div>}
    </div>
  );
}
