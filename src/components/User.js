import React from 'react';

function User({ user, message }) {
  return (
    <div>
      <span>{message}</span>
      <span>{user.name}</span>
      <span>{user.admin}</span>
    </div>
  );
}

export default User;
