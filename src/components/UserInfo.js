import React, { useState } from "react";
import { useQuery, gql } from '@apollo/client';
import User from './User';

const GET_USER = gql`
  query User($name: String) {
    user(name: $name) {
      name
      admin
    }
  }
`;

function UserInfo({ }) {
  const [name, setName] = useState(null);

  const { loading, error, data, refetch } = useQuery(GET_USER, {
    variables: { name }
  });

  return (
    <div>
      {data && data.user && <User user={data.user} message="User Info" />}
      <input type="text" name="name" onChange={(e) => { setName(e.target.value); }} placeholder="name"/>
      <button onClick={() => refetch()}>Get User</button>
    </div>
  );
}

export default UserInfo;
