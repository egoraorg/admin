import React, { useState } from 'react';
import { gql, ApolloClient, useApolloClient, useMutation } from "@apollo/client";

const DELETE_COMMUNITY = gql`
  mutation DeleteCommunity($groupId: String) {
    deleteCommunity(groupId: $groupId)
  }
`;

export default function DeleteCommunity({  }) {
  const [show, setShow] = useState(false);
  const [groupId, setGroupId] = useState(null);
  const [isDeleted, setIsDeleted] = useState(null);
  const client = useApolloClient();
  
  const [deleteCommunity, { loading, error }] = useMutation(DELETE_COMMUNITY, {
    onCompleted({ deleteCommunity }) {
      setIsDeleted(true);
    },
    onError(err) {
      setIsDeleted(false);
    },
  });

  return (
    <div> 
      <span>Show Delete Community</span>
      <input type="checkbox" name="show" value={show} onChange={(e) => { setShow(e.target.value); }} placeholder="show"/>
      {show && <div> 
        <input type="text" name="groupId" onChange={(e) => { setGroupId(e.target.value); }} placeholder="groupId"/>
        <button onClick={() => deleteCommunity({ variables: { groupId } })}>Delete</button>
        {isDeleted != null && <div> 
          <span>Is Deleted: {isDeleted} </span>
        </div>}
      </div>}
    </div>
  );
}
