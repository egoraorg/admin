import React, { useState } from 'react';
import { gql, ApolloClient, useApolloClient, useMutation } from "@apollo/client";

const DEACTIVATE_USER = gql`
  mutation DeactivateUser($name: String) {
    deactivateUser(name: $name)
  }
`;

export default function DeactivateUser({  }) {
  const [show, setShow] = useState(false);
  const [name, setName] = useState(null);
  const [isDeactivated, setIsDeactivated] = useState(false);
  const client = useApolloClient();
  
  const [deactivateUser, { loading, error }] = useMutation(DEACTIVATE_USER, {
    onCompleted({ deactivateUser }) {
      setIsDeactivated(deactivateUser);
    }
  });

  return (
    <div> 
      <span>Show Deactivate</span>
      <input type="checkbox" name="show" value={show} onChange={(e) => { setShow(e.target.value); }} placeholder="show"/>
      {show && <div> 
        <input type="text" name="name" onChange={(e) => { setName(e.target.value); }} placeholder="name"/>
        <button onClick={() => deactivateUser({ variables: { name } })}>Deactivate</button>
        <span>Deactivated: {isDeactivated}</span>
      </div>}
    </div>
  );
}
