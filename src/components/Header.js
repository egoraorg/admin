import React from 'react';

function Header({ user }) {
  return (
    <div>
      <h1>Admin</h1> 
      {user && <div>
        <h2>Logged in as {user.name}</h2>
      </div>}
    </div>
  );
}

export default Header;
