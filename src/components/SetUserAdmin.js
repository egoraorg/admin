import React, { useState } from 'react';
import { gql, ApolloClient, useApolloClient, useMutation } from "@apollo/client";

const SET_USER_ADMIN = gql`
  mutation SetUserAdmin($name: String, $admin: Boolean) {
    setUserAdmin(name: $name, admin: $admin) {
      name
      admin
    }
  }
`;

export default function SetUserAdmin({  }) {
  const [name, setName] = useState(null);
  const [isAdmin, setIsAdmin] = useState(false);
  const [user, setUser] = useState(null);
  const client = useApolloClient();
  
  const [setUserAdmin, { loading, error }] = useMutation(SET_USER_ADMIN, {
    onCompleted({ user }) {
      setUser(user);
    }
  });

  return (
    <div> 
      <input type="text" name="name" onChange={(e) => { setName(e.target.value); }} placeholder="name"/>
      <input type="checkbox" name="isAdmin" value={isAdmin} onChange={(e) => { setIsAdmin(e.target.value); }} placeholder="isAdmin"/>
      <button onClick={() => setUserAdmin({ variables: { name, admin: isAdmin } })}>Set User Admin</button>
      <span>User: {user}</span>
    </div>
  );
}
