import React from 'react';
import { useQuery, gql } from '@apollo/client';

const GET_USERS = gql`
  {
    users {
      name
      admin
    }
  }
`;

function Users({ onUserSelected }) {
  const { loading, error, data } = useQuery(GET_USERS);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <select name="user" onChange={onUserSelected}>
      {data.users.map(user => (
        <option key={user.name} value={user.name}>
          {user.name}
        </option>
      ))}
    </select>
  )
}

export default Users;
