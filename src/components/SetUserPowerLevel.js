import React, { useState } from 'react';
import { gql, ApolloClient, useApolloClient, useMutation } from "@apollo/client";

const SET_USER_POWER_LEVEL= gql`
  mutation SetUserPowerLevel($roomId: String, $name: String, $powerLevel: Int) {
    setUserPowerLevel(roomId: $roomId, name: $name, powerLevel: $powerLevel) {
      event_id
    }
  }
`;

export default function SetUserAdmin({  }) {
  const [roomId, setRoomId] = useState(null);
  const [name, setName] = useState(null);
  const [powerLevel, setPowerLevel] = useState(0);
  const [event, setEvent] = useState(null);
  const client = useApolloClient();
  
  const [setUserPowerLevel, { loading, error }] = useMutation(SET_USER_POWER_LEVEL, {
    onCompleted({ event }) {
      setEvent(event);
    }
  });

  return (
    <div> 
      <input type="text" name="roomId" onChange={(e) => { setRoomId(e.target.value); }} placeholder="roomId"/>
      <input type="text" name="name" onChange={(e) => { setName(e.target.value); }} placeholder="name"/>
      <input type="number" name="powerLevel" value={powerLevel} onChange={(e) => { setPowerLevel(e.target.value); }} placeholder="powerLevel"/>
      <button onClick={() => setUserPowerLevel({ variables: { roomId, name, powerLevel: Number.parseInt(powerLevel, 10) } })}>Set User Power Level</button>
      <span>Event ID: {event}</span>
    </div>
  );
}
