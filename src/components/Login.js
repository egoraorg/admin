import React, { useState } from 'react';
import { gql, ApolloClient, useApolloClient, useMutation } from "@apollo/client";

const LOGIN_USER = gql`
  mutation Login($name: String, $password: String) {
    login(name: $name, password: $password) {
      access_token
      home_server
      user_id
    }
  }
`;

export default function Login({ onLogin }) {
  const [name, setName] = useState(null);
  const [password, setPassword] = useState(null);

  const client = useApolloClient();
  const [login, { loading, error }] = useMutation(LOGIN_USER, {
    onCompleted({ login }) {
      localStorage.setItem("accessToken", login.accessToken);
      //client.writeData({ data: { isLoggedIn: true } });
      onLogin(login);
    }
  });

  return (
    <div>
      <input type="text" name="name" onChange={(e) => { setName(e.target.value); }} placeholder="name"/>
      <input type="password" name="password" onChange={(e) => { setPassword(e.target.value); }} placeholder="password"/>
      <button onClick={() => login({ variables: { name, password } })}>Login</button>
    </div>
  );
}
